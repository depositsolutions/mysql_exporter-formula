# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "mysql_exporter/map.jinja" import mysql_exporter with context %}

mysql_exporter-remove-service:
  service.dead:
    - name: mysql_exporter
  file.absent:
    - name: /etc/systemd/system/mysql_exporter.service
    - require:
      - service: mysql_exporter-remove-service
  cmd.run:
    - name: 'systemctl daemon-reload'
    - onchanges:
      - file: mysql_exporter-remove-service

mysql_exporter-remove-symlink:
   file.absent:
    - name: {{ mysql_exporter.bin_dir}}/mysql_exporter
    - require:
      - mysql_exporter-remove-service

mysql_exporter-remove-binary:
   file.absent:
    - name: {{ mysql_exporter.dist_dir}}
    - require:
      - mysql_exporter-remove-symlink

mysql_exporter-remove-config:
    file.absent:
      - name: {{ mysql_exporter.config_dir }}
      - require:
        - mysql_exporter-remove-binary
